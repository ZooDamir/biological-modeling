import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import numpy as np
from tkinter import *
from tkinter import ttk

matplotlib.use("TkAgg")
plt.style.use('ggplot')

x0 = 1
y0 = 1
alpha1 = 0.5
alpha2 = 0.3
beta = 0.4
k = 0.8
delta_t = 0.25
N = 200


def f1(x, y):
    """
    Speed of change in victim population
    :param x: number of victims
    :param y: number of predators
    :return: Current speed of change in victim population
    """
    return alpha1 * x - beta * x * y


def f2(x, y):
    """
    Speed  of change in predator population
    :param x: number of victims
    :param y: number of predators
    :return: Current speed of change in predator population
    """
    return -alpha2 * y + k * beta * x * y


def generate_t_array():
    """
    Helping function to generate array of discrete moments of time evenly spaced from 0
    :return: array of points in time
    """
    return np.linspace(0, N * delta_t, num=N, endpoint=False)


def euler():
    """
    Numerical method to calculate population size in discrete moments of time
    :return: Population of victims, Population of predators
    """
    x_plt = []
    y_plt = []
    x = x0
    y = y0
    x_plt.append(x)
    y_plt.append(y)
    for j in range(1, N):
        temp1 = f1(x, y)
        temp2 = f2(x, y)

        x = x + delta_t * temp1
        y = y + delta_t * temp2

        x_plt.append(x)
        y_plt.append(y)

    return x_plt, y_plt


def predictor_corrector():
    """
    Numerical method to calculate population size in discrete moments of time
    :return: Population of victims, Population of predators
    """
    x_plt = []
    y_plt = []
    x = x0
    y = y0
    x_plt.append(x)
    y_plt.append(y)

    for j in range(1, N):
        temp1 = f1(x, y)
        temp2 = f2(x, y)
        x_ = x + delta_t / 2 * temp1
        y_ = y + delta_t / 2 * temp2

        temp1 = f1(x_, y_)
        temp2 = f2(x_, y_)

        x = x + delta_t * temp1
        y = y + delta_t * temp2

        x_plt.append(x)
        y_plt.append(y)

    return x_plt, y_plt


# ====================================
# ===========USER INTERFACE===========
# ====================================
root = Tk()
root.title('Math modeling of Victim-Predator system')

x0_label = Label(root, text='Enter x0 :')
x0_label.grid(row=1, column=1, sticky=E, pady=5)
x0_input = Entry(root, bd=1)
x0_input.grid(row=1, column=2)
x0_input.insert(0, '1')

y0_label = Label(root, text='Enter y0 :')
y0_label.grid(row=2, column=1, sticky=E, pady=5)
y0_input = Entry(root, bd=1)
y0_input.grid(row=2, column=2)
y0_input.insert(0, '1')

alpha1_label = Label(root, text='Enter 𝛂1 :')
alpha1_label.grid(row=3, column=1, sticky=E, pady=5)
alpha1_input = Entry(root, bd=1)
alpha1_input.grid(row=3, column=2)
alpha1_input.insert(0, '0.5')

alpha2_label = Label(root, text='Enter 𝛂2 :')
alpha2_label.grid(row=4, column=1, sticky=E, pady=5)
alpha2_input = Entry(root, bd=1)
alpha2_input.grid(row=4, column=2)
alpha2_input.insert(0, '0.3')

beta_label = Label(root, text='Enter 𝛃 :')
beta_label.grid(row=5, column=1, sticky=E, pady=5)
beta_input = Entry(root, bd=1)
beta_input.grid(row=5, column=2, padx=5)
beta_input.insert(0, '0.4')

k_label = Label(root, text='Enter k :')
k_label.grid(row=6, column=1, sticky=E, pady=5)
k_input = Entry(root, bd=1)
k_input.grid(row=6, column=2)
k_input.insert(0, '0.8')

delta_t_label = Label(root, text='Enter Δt :')
delta_t_label.grid(row=7, column=1, sticky=E, pady=5)
delta_t_input = Entry(root, bd=1)
delta_t_input.grid(row=7, column=2)
delta_t_input.insert(0, '0.25')

N_label = Label(root, text='Enter N :')
N_label.grid(row=8, column=1, sticky=E, pady=5)
N_input = Entry(root, bd=1)
N_input.grid(row=8, column=2)
N_input.insert(0, '200')

tabs = ttk.Notebook(root)
euler_yx_tab = ttk.Frame(tabs)
euler_t_tab = ttk.Frame(tabs)
predictor_corrector_yx_tab = ttk.Frame(tabs)
predictor_corrector_t_tab = ttk.Frame(tabs)
tabs.add(euler_yx_tab, text='Euler y(x)')
tabs.add(euler_t_tab, text='Euler x(t), y(t)')
tabs.add(predictor_corrector_yx_tab, text='Predictor-Corrector y(x)')
tabs.add(predictor_corrector_t_tab, text='Predictor-Corrector x(t), y(t)')
tabs.grid(row=1, column=3, rowspan=9, sticky=E)

figure_euler_yx = Figure(figsize=(10, 5), dpi=100)
ax_euler_yx = figure_euler_yx.add_subplot(111)
graph_euler_yx = FigureCanvasTkAgg(figure_euler_yx, euler_yx_tab)
graph_euler_yx.get_tk_widget().grid(row=1, column=3, rowspan=9, padx=0)

figure_euler_t = Figure(figsize=(10, 5), dpi=100)
ax_euler_t = figure_euler_t.add_subplot(111)
graph_euler_t = FigureCanvasTkAgg(figure_euler_t, euler_t_tab)
graph_euler_t.get_tk_widget().grid(row=1, column=3, rowspan=9, padx=0)

figure_predictor_corrector_yx = Figure(figsize=(10, 5), dpi=100)
ax_predictor_corrector_yx = figure_predictor_corrector_yx.add_subplot(111)
graph_predictor_corrector_yx = FigureCanvasTkAgg(figure_predictor_corrector_yx, predictor_corrector_yx_tab)
graph_predictor_corrector_yx.get_tk_widget().grid(row=1, column=3, rowspan=9, padx=0)

figure_predictor_corrector_t = Figure(figsize=(10, 5), dpi=100)
ax_predictor_corrector_t = figure_predictor_corrector_t.add_subplot(111)
graph_predictor_corrector_t = FigureCanvasTkAgg(figure_predictor_corrector_t, predictor_corrector_t_tab)
graph_predictor_corrector_t.get_tk_widget().grid(row=1, column=3, rowspan=9, padx=0)


def plot_solution_without_animation():
    global x0, y0, alpha1, alpha2, beta, k, delta_t, N
    x0 = float(x0_input.get() or '1')
    y0 = float(y0_input.get() or '1')
    alpha1 = float(alpha1_input.get() or '0.5')
    alpha2 = float(alpha2_input.get() or '0.3')
    beta = float(beta_input.get() or '0.4')
    k = float(k_input.get() or '0.8')
    delta_t = float(delta_t_input.get() or '0.25')
    N = int(N_input.get() or '200')

    euler_x, euler_y = euler()
    predictor_corrector_x, predictor_corrector_y = predictor_corrector()
    t = generate_t_array()

    ax_euler_yx.clear()
    ax_euler_t.clear()
    ax_predictor_corrector_yx.clear()
    ax_predictor_corrector_t.clear()

    ax_euler_yx.plot(euler_x, euler_y, '-', label='y(x)')
    ax_euler_yx.legend()
    graph_euler_yx.draw()

    ax_euler_t.plot(t, euler_x, '-', label='x(t)')
    ax_euler_t.plot(t, euler_y, '-', label='y(t)')
    ax_euler_t.legend()
    graph_euler_t.draw()

    ax_predictor_corrector_yx.plot(predictor_corrector_x, predictor_corrector_y, '-', label='y(x)')
    ax_predictor_corrector_yx.legend()
    graph_predictor_corrector_yx.draw()

    ax_predictor_corrector_t.plot(t, predictor_corrector_x, '-', label='x(t)')
    ax_predictor_corrector_t.plot(t, predictor_corrector_y, '-', label='y(t)')
    ax_predictor_corrector_t.legend()
    graph_predictor_corrector_t.draw()

    del euler_x
    del euler_y
    del predictor_corrector_x
    del predictor_corrector_y
    del t

    return


calculate_button = Button(text='Calculate system', command=plot_solution_without_animation)
calculate_button.grid(row=9, column=1, columnspan=2, pady=5)

root.mainloop()
